#define IA_HPP
#ifdef IA_HPP
#include "Grille.hpp"
#include "Joueur.hpp"

class IA : public Joueur
{
public:
    IA(int color);
    bool placePawn(const int x, const int y, Grille *board);
    ~IA();
};

#endif