#ifndef JEU_HPP
#define JEU_HPP

#include <vector>
#include <iostream>
#include "Humain.hpp"
#include "Grille.hpp"
#include "Pion.hpp"
#include "IA.hpp"

class Jeu
{
private:
    int _nbTour;
    std::vector<Joueur*> _joueurs;
    Grille _grille;
    std::string _gagnant;
    bool checkPawnPosition(int x, int y) const;
public:
    Jeu(int nbTour, Grille grille);
    void start();
    void startWithIa();
    void play();
    void playWithIa();
    void stop();
    bool victory();
    void menu();
    ~Jeu();
};

#endif

