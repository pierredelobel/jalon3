#include "Jeu.hpp"

Jeu::Jeu(int nbTour, Grille grille) : _nbTour(nbTour), _grille(grille)
{
    this->_grille.reset();
}

void Jeu::start()
{
    char nom[100];
    std::cout << "\tQuel est le nom du joueur 1 ? ";
    std::cin >> nom;
    this->_joueurs.push_back(new Humain(nom, 1));

    std::cout << "\tQuel est le nom du joueur 2 ? ";
    std::cin >> nom;
    this->_joueurs.push_back(new Humain(nom, 2));

    std::cout << std::endl;
    this->play();
}

void Jeu::startWithIa()
{
    char nom[100];
    std::cout << "\tQuel est le nom du joueur 1 ? ";
    std::cin >> nom;
    this->_joueurs.push_back(new Humain(nom, 2));
    this->_joueurs.push_back(new IA(1));
    this->playWithIa();
}

void Jeu::menu()
{
    std::cout << std::endl;
    std::cout << "\tBienvenu sur Gomoku !" << std::endl;
    std::cout << "\t---------------------" << std::endl;
    std::cout << "\tTapez sur :" << std::endl;
    std::cout << "\t1) pour jouer contre un joueur" << std::endl;
    std::cout << "\t2) pour jouer contre l'IA" << std::endl;
    std::cout << "\t3) pour quitter" << std::endl;

    int choix = 0;
    std::cout << "\t";
    std::cin >> choix;
    while (!std::cin)
    {
        std::cin.clear();
        std::cin.ignore(10, '\n');
        std::cout << "\tVeuillez entrer un nombre" << std::endl;
        std::cin >> choix;
    }

    while (choix != 1 && choix != 2)
    {
        std::cout << "\tVeuillez entrer un nombre présent dans le menu" << std::endl;
        std::cin >> choix;
    }

    switch (choix)
    {
    case 1:
        this->start();
        break;
    case 2:
        this->startWithIa();
        break;

    case 3:
        this->stop();
        break;

    default:
        break;
    }
}

void Jeu::play()
{
    this->_grille.changeValue(9, 9, (*this->_joueurs[_nbTour % 2]).getColor());
    this->_nbTour++;
    int abscisse = -1, ordonnee = -1;
    char rejouer;
    while (!this->victory() && !this->_grille.fullBoard())
    {
        std::cout << "\tAu tour de " << (*this->_joueurs[_nbTour % 2]).getName() << std::endl
                  << std::endl;
        this->_grille.showBoard();
        std::cout << "\tPlacement en i ? ";
        std::cin >> abscisse;
        std::cout << "\tPlacement en j ? ";
        std::cin >> ordonnee;
        bool res = (*this->_joueurs[_nbTour % 2]).placePawn(abscisse, ordonnee, &_grille);
        if (!res)
        {
            std::cout << "\tCoup non valable. Rejouez" << std::endl;
            this->_nbTour--;
        }

        abscisse = -1;
        ordonnee = -1;

        this->_nbTour++;
    }

    if (this->victory()) {
        std::cout << "\tLe joueur " << this->_gagnant << " a gagné !" << std::endl;
    }

    if (this->_grille.fullBoard()) {
        std::cout << "\tEgalite ! La grille est pleine" << std::endl;
    }
    
    
    std::cout << std::endl;
    std::cout << "Voulez vous rejouer ? (o : oui, n : non, e : exit)" << std::endl;
    std::cin >> rejouer;
    switch (rejouer)
    {
    case 'o':
        this->start();
        break;

    case 'n':
        this->menu();
        break;

    default:
        this->stop();
        break;
    }
}

void Jeu::playWithIa()
{
    std::cout << "play with ia" << std::endl;
    this->_grille.changeValue(9, 9, (*this->_joueurs[_nbTour % 2]).getColor());
    this->_nbTour++;
    int abscisse = -1, ordonnee = -1;
    char rejouer;
    this->_grille.showBoard();
    while (!this->victory() && !this->_grille.fullBoard()) {
        if (_nbTour % 2 == 0)
        {
            std::cout << "\tAu tour de " << (*this->_joueurs[_nbTour % 2]).getName() << std::endl
                  << std::endl;
            this->_grille.showBoard();
            std::cout << "\tPlacement en i ? ";
            std::cin >> abscisse;
            std::cout << "\tPlacement en j ? ";
            std::cin >> ordonnee;
            bool res = (*this->_joueurs[_nbTour % 2]).placePawn(abscisse, ordonnee, &_grille);
            if (!res)
            {
                std::cout << "\tCoup non valable. Rejouez" << std::endl;
                this->_nbTour--;
            }

            abscisse = -1;
            ordonnee = -1;

            this->_nbTour++;
        }
        else
        {
            (*this->_joueurs[_nbTour % 2]).placePawn(0, 0, &_grille);
            _nbTour++;
        }
        
    }

}

void Jeu::stop()
{
    exit(EXIT_SUCCESS);
}

bool Jeu::victory()
{
    int res = this->_grille.checkVictory();
    if (res == 0)
    {
        return false;
    }
    // this->_gagnant =
    std::vector<Joueur*>::const_iterator iterator = this->_joueurs.begin();
    while (iterator != _joueurs.end())
    {
        if ((*iterator)->getColor() == res)
        {
            this->_gagnant = (*iterator)->getName();
        }
        iterator++;
    }

    this->_grille.reset();
    return true;
}

// check si l'emplacement demandé est dans la grille
bool Jeu::checkPawnPosition(int x, int y) const
{
    if (x > 18 || y > 18 || x < -1 || y < -1)
    {
        return false;
    }
    return true;
}

Jeu::~Jeu()
{
    std::vector<Joueur*>::iterator iterator = this->_joueurs.begin();
    while (iterator != this->_joueurs.end())
    {
        delete *(iterator);
    }
}
