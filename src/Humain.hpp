#define HUMAIN_HPP
#ifdef HUMAIN_HPP
#include "Joueur.hpp"
#include "Grille.hpp"

class Humain : public Joueur
{
public:
    Humain(std::string name, int color);
    bool placePawn(const int x, const int y, Grille *board);
    ~Humain();
};

#endif

