#ifndef GRILLE_HPP
#define GRILLE_HPP
#include <iostream>
#include <vector>
int const static DIMX =  19;
int const static DIMY =  19;

class Grille{
    private:
        int _board[DIMX][DIMY];
        int checkHorizontal();
        int checkVertical();
        int checkDiagonal();

    public:
        void reset();
        void showBoard() const;
        bool freePlace(const int x, const int y) const;
        void changeValue(const int x,const int y,const int color);
        int checkVictory();
        bool fullBoard();
        std::vector<std::pair<int, int>> getFreePlaces() const;
        ~Grille();
};

#endif
