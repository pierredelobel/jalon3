#include "IA.hpp"

IA::IA(int color) : Joueur("IA", color)
{}

bool IA::placePawn(const int x, const int y, Grille *board)
{
    std::vector<std::pair<int, int>> freePlaces = board->getFreePlaces();
    std::vector<std::pair<int, int>>::const_iterator iterator = freePlaces.begin();
    int rand = std::rand()%(freePlaces.size()+1);
    board->changeValue(freePlaces[rand].first, freePlaces[rand].second, this->getColor());
    return true;
}