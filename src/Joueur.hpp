#ifndef JOUEUR_HPP
#define JOUEUR_HPP

#include <iostream>
#include <vector>

class Grille;

class Joueur{
    private:
        std::string _name;
        int _color;
    public:
        Joueur(std::string name, int color);
        virtual bool placePawn(const int x, const int y, Grille *board) = 0;
        const std::string &getName() const;
        const int &getColor() const;
};

#endif
