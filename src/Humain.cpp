#include "Humain.hpp"

Humain::Humain(std::string name, int color) : Joueur(name, color)
{}

bool Humain::placePawn(const int x, const int y, Grille *board)
{
    if(board->freePlace(x,y))
    {
      board->changeValue(x,y, Joueur::getColor());
      return true;
    }
    return false;
}