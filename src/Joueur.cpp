#include "Joueur.hpp"
#include <iostream>
#include <vector>
#include "Grille.hpp"

Joueur::Joueur(std::string name, int color): _name(name),_color(color) {};

const std::string &Joueur::getName() const
{
  return this->_name;
}

const int &Joueur::getColor() const
{
  return this->_color;
}
