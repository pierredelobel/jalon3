#include <iostream>
#include "Grille.hpp"

void Grille::reset()
{
    for(int i = 0; i < DIMX; i++)
        for(int j = 0 ; j < DIMY; j++){
            _board[i][j] = 0;
        }
}

void Grille::showBoard() const {
    for (int i = 0; i < DIMX; i++){
        for (int j = 0; j < DIMX; j++) {
            std::cout << _board[i][j] << " ";
        }
        std::cout << std::endl;
    }
};

bool Grille::freePlace(const int x, const int y) const
{
    if(_board[x][y] != 0)
    {
      return false;
    }
    else
    {
      if(_board[x-1][y-1] != 0 || _board[x-1][y] != 0 || _board[x-1][y+1] != 0
      || _board[x][y-1] != 0 || _board[x][y+1] != 0
      || _board[x+1][y-1] != 0 || _board[x+1][y] != 0 || _board[x+1][y+1] != 0){
        return true;
      }
      return false;
    }
};


void Grille::changeValue(const int x,const int y,const int color)
{
  _board[x][y] = color;
};

bool Grille::fullBoard()
{
  for (int i = 0; i < DIMX; i++){
      for (int j = 0; j < DIMX; j++) {
        if(this->_board[i][j] == 0 )
          return false;
      }
  }
  return true;
}

int Grille::checkVictory()
{
  int result = checkVertical();

  if(result == 0)
    result =  checkHorizontal();

  if(result == 0)
    result = checkDiagonal();

  return result;
}

int Grille::checkHorizontal()
{
  for(int i = 0; i < DIMX; i++){
    for(int j = 0; j < DIMY - 4 ; j++){
      int currentColor = _board[i][j];
      if(currentColor != 0 && _board[i][j+1] == currentColor &&  _board[i][j+2] == currentColor &&  _board[i][j+3] == currentColor &&  _board[i][j+4] == currentColor)
        return currentColor;
    }
  }
  return 0;
}

int Grille::checkVertical()
{
  for(int i = 0; i < DIMX - 4; i++){
    for(int j = 0; j < DIMY; j++){
      int currentColor = _board[i][j];
      if(currentColor != 0 && _board[i+1][j] == currentColor &&  _board[i+2][j] == currentColor &&  _board[i +3 ][j] == currentColor &&  _board[i +4 ][j] == currentColor)
        return currentColor;
    }
  }
  return 0;
}

int Grille::checkDiagonal()
{
  for(int i = 2 ; i < DIMX - 2; i++){
    for(int j = 2; j < DIMY - 2 ; j++){
      int currentColor = _board[i][j];

      if(currentColor != 0 && (
        (_board[i+1][j+1] == currentColor && _board[i+2][j+2] == currentColor && _board[i-1][j-1] == currentColor && _board[i-2][j-2] == currentColor) ||
        (_board[i+1][j-1] == currentColor && _board[i+2][j-2] == currentColor && _board[i-1][j+1] == currentColor && _board[i-2][j+2] == currentColor )))
          return currentColor;
    }
  }
  return 0;
}

std::vector<std::pair<int, int>> Grille::getFreePlaces() const
{
  std::vector<std::pair<int, int>> freePlaces;
  for(int i = 0; i < DIMX-1; i++){
    for(int j = 0; j < DIMY-1; j++){
      if (i != 0 && j != 0)
      {
        if (_board[i][j] == 0 && 
        (
          _board[i-1][j-1] != 0 || _board[i-1][j] != 0 || _board[i-1][j+1] != 0 ||
          _board[i][j-1] != 0 || _board[i][j] != 0 || _board[i][j+1] != 0||
          _board[i+1][j-1] != 0 || _board[i+1][j] != 0 || _board[i+1][j+1] != 0
        )) {
          freePlaces.push_back(std::make_pair(i, j));
        }
      } else if (i == 0 && j != 0) {
        if (_board[i][j] == 0 && 
        (
          _board[i][j-1] != 0 || _board[i][j] != 0 || _board[i][j+1] != 0||
          _board[i+1][j-1] != 0 || _board[i+1][j] != 0 || _board[i+1][j+1] != 0
        )) {
          freePlaces.push_back(std::make_pair(i, j));
        }
      } else if (j == 0 && i != 0)
      {
        if (_board[i][j] == 0 && 
        (
          _board[i-1][j] != 0 || _board[i-1][j+1] != 0 ||
          _board[i][j] != 0 || _board[i][j+1] != 0||
          _board[i+1][j] != 0 || _board[i+1][j+1] != 0
        )) {
          freePlaces.push_back(std::make_pair(i, j));
        }
      } else if (i == 0 && j == 0) {
        if (_board[i][j] == 0 && 
        (
          _board[i][j] != 0 || _board[i][j+1] != 0||
          _board[i+1][j] != 0 || _board[i+1][j+1] != 0
        )) {
          freePlaces.push_back(std::make_pair(i, j));
        }
      }
      
      
    }
  }
  return freePlaces;
}

Grille::~Grille(){};
