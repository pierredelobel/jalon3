#include <iostream>
#include "Joueur.hpp"
#include "Grille.hpp"
#include "Jeu.hpp"

int main()
{
    Grille g;
    //g.reset();
    /*Joueur toto("toto", 1);
    Joueur tata("tata", 2);*/
    Jeu jeu(0, g);
    jeu.menu();
    return 0;
}